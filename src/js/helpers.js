const convertImages = (query, callback) => {
    const images = document.querySelectorAll(query)

    images.forEach(image => {
        fetch(image.src)
            .then(res => res.text())
            .then(data => {
                const parser = new DOMParser()
                const svg = parser.parseFromString(data, 'image/svg+xml').querySelector('svg')

                if (svg) {
                    if (image.id) svg.id = image.id
                    if (image.className) svg.classList = image.classList
                    image.parentNode.replaceChild(svg, image)
                }
            })
            .then(callback)
            .catch(error => console.error(error))
    })
}

convertImages('.svg', function () {
    console.log('success')
});

AOS.init({
    duration: 800,
    easing: 'easy-in-out',
    once: true,
    offset: -20
})

$('.reviews-slider .card-btn').on('click', function () {
    const item = $(this);
    const desc = item.parents('.card').find('.default-desc');
    desc.toggleClass('desc-clamp');
    desc.hasClass('desc-clamp') ? item.text('Показать полностью') : item.text('Скрыть')
})

function setToggler(selector) {
    const items = $(selector)
    items.on('click', function () {
        const item = $(this);
        if(item.hasClass('active')) {
            item.removeClass('active')
            item.find('.default-desc').slideUp('slow');
        } else {
            items.removeClass('active')
            items.find('.default-desc').slideUp('slow')
            item.addClass('active');
            item.find('.default-desc').slideDown('slow');
        }
    })
}

setToggler('.faq-items .item')

function toggleModal(btn) {
    $(btn).on('click', function () {
        const modal = $($(this).attr('data-modal'));
        const html = $('html');
        modal.addClass('active');
        modal.find('.success').remove()
        html.addClass('active');
        modal.on('click', function (e) {
            const target = $(e.target);
            if(target.hasClass('close-modal') || target.hasClass('active')) {
                modal.removeClass('active')
                html.removeClass('active');
            }
        })
    })
}

toggleModal('button[data-modal]')

function closeModal(selector) {
    $(selector).on('click', function () {
        const item = $(this);
        item.parents('.modal-success').removeClass('active');
    })
}

closeModal('.close-success')

$('input[type="phone"]').mask('+38 (999) 999 99 99', {placeholder: "+38 (ХХХ) XХХ ХХ XX" })

function changeTeacherPhoto () {
    const slides = $('.teachers-slider .slide');
    function change(slide) {
        const wrap = slide.find('.teacher-photo');
        const img = slide.find('.teacher-photo img');
        const newImg = img.attr('src');
        const current = wrap.attr('data-photo');
        wrap.attr('data-photo', newImg);
        img.attr('src', current)
    }
    slides.on('mouseenter', function () {
        const slide = $(this);
        change(slide);
    })

    slides.on('mouseleave', function () {
        const slide = $(this);
        change(slide);
    })
}

changeTeacherPhoto()