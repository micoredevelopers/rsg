function setLanguage() {
    $('.header-language__switcher').on('click', function () {
        $(this).toggleClass('active');
    })
}

setLanguage()

function toggleMenu() {
    $('.burger-menu').on('click', function () {
        $(this).toggleClass('active');
        $('.header-nav').toggleClass('active');
    })
}

toggleMenu()

function toggleServices(event) {
    $('.header-nav__item-menu').on('click', function () {
        const item = $(this);
        const height = item.find('.header-nav__services').height();
        const wrapper = item.find('.header-nav__services-wrap');
        item.toggleClass('active');

        if(item.hasClass('active')) {
            wrapper.css('maxHeight', `${height + 15}px`)
        } else {
            wrapper.css('maxHeight', `${0}px`)
        }

    })
}

if(window.innerWidth < 993) {
    toggleServices()
}

function setScrollEvent() {
    $(window).on('scroll', (e) => {
        let trigger = 0;
        window.innerWidth < 992 ? trigger = 30 : trigger = 100;
        if(window.scrollY > trigger) {
            AOS.refresh({
                duration: 800, // values from 0 to 3000, with step 50ms
                easing: 'easy-in-out',
                once: true,
                offset: -20
            })
        }

        const yPos = -(window.scrollY * 10 / 100);

        if(innerWidth > 992) {
            $('.section-img2, .section-img, .section-img3, .section-img4, .courses-img, .courses-img2').attr('style','transform: translate3d(0px, ' + yPos + 'px, 0px)')
        }

    })
}

setScrollEvent()