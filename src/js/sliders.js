function options (next, prev, dots) {
    return {
        mainSection: {
            fade: true,
            speed: 600,
            arrows: true,
            dots: true,
            nextArrow: $(next),
            prevArrow: $(prev),
            appendDots:$(dots)
        },
        teacherSlider: {
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: true,
            arrows: true,
            infinite: true,
            nextArrow: $(next),
            prevArrow: $(prev),
            appendDots: $(dots),
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: false
                }

            }]
        },
        reviewsSlider: {
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true,
            arrows: true,
            infinite: true,
            nextArrow: $(next),
            prevArrow: $(prev),
            appendDots: $(dots),
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: false
                }

            }]
        },
        serviceSliderSM: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            infinite: true
        },
        serviceSliderXL: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            infinite: true,
            nextArrow: $(next),
            prevArrow: $(prev),
            appendDots: $(dots),
        }
    }
}

function setSlider(selector, options) {
    $(selector).slick(options);
}

setSlider('.main-section__slider', options('.main-section__slider-wrap .slider-arrow__next', '.main-section__slider-wrap .slider-arrow__prev', '.slider-dots').mainSection)
setSlider('.teachers-slider', options('#teachers-section .slider-arrow__next', '#teachers-section .slider-arrow__prev', '#teachers-section .slider-dots').teacherSlider)
setSlider('.reviews-slider', options('#reviews-section .slider-arrow__next', '#reviews-section .slider-arrow__prev', '#reviews-section .slider-dots').reviewsSlider)
if(window.innerWidth < 993) {
    setSlider('.gallery-slider__sm', options().serviceSliderSM)
} else {
    setSlider('.gallery-slider__xl', options('#gallery-section .slider-arrow__next', '#gallery-section .slider-arrow__prev', '#gallery-section .slider-dots').serviceSliderXL)
}