function ajaxSending(selector) {
    let time = null;
    $(selector).on('submit', (e) => {
        e.preventDefault();
        const form = $(e.target);
        const btn = form.find('.default-btn');


        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            success: function(res) {
                form.find('.input-error').remove()
                form.find('.success').remove()
                form.append(`<div class="success">Отправлено!</div>`)
            },
            error: function(res) {
                const errors = res.responseJSON.errors;
                let error = null;
                form.find('.input-error').remove()

                for (key in errors) {
                    for (let i = 0; i < errors[key].length; i++) {
                        error += errors[key]
                    }
                    const template = `<div class="input-error">${error}</div>`

                    if(key !== '#') {
                        form.append(template)
                    }

                    error = ''
                }

                time = setTimeout(function () {
                    form.find('.input-error').remove()
                }, 5000)
            }
        })
    })
}

ajaxSending('.default-form')